import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',

    state: {
      availableClasses: [],
      selectedClasses: [],
      classesReady: false,

      hierarchy: [],
      hierarchyReady: false,

      loadingRoot: true
    },

    getters: {
      nothingSelected(state) {
        return state.selectedClasses.length === 0
      }
    },

    mutations: {
      initializeClasses(state, { classes }) {
        state.availableClasses = classes
        state.selectedClasses = state.availableClasses.map(c => c.id_class)
        state.classesReady = true
      },

      updateSelectedClasses(state, { classIds }) {
        state.selectedClasses = classIds
      },

      beginLoadingRoot(state) {
        state.loadingRoot = true
      },

      endLoadingRoot(state) {
        state.loadingRoot = false
      },

      beginUpdateOfHierarchy(state, { parent }) {
        state.hierarchy = state.hierarchy.slice(0, parent.level)
        state.hierarchy.push({ parent, children: [] })

        state.hierarchyReady = false
      },

      endUpdateOfHierarchy(state, { children }) {
        const last = state.hierarchy[state.hierarchy.length - 1]

        last.children = children.map(taxon => ({
          ...taxon,
          level: last.parent.level + 1
        }))

        state.hierarchyReady = true
      }
    },

    actions: {
      async initializeClasses({ commit, dispatch }) {
        const classes = await this.$axios.$get('/nr2/classes')
        commit('initializeClasses', { classes })
      },

      selectAllClasses({ state, commit }) {
        commit('updateSelectedClasses', {
          classIds: state.availableClasses.map(c => c.id_class)
        })
      },

      clearClasses({ commit }) {
        commit('updateSelectedClasses', { classIds: [] })
      },

      async updateSelectedClasses({ state, commit, dispatch }, { classIds }) {
        commit('updateSelectedClasses', { classIds })
        await dispatch('refreshHierarchy')
      },

      async initializeHierarchy({ dispatch }) {
        await dispatch('refreshHierarchy')
        // const roots = await this.$axios.$get("/root_taxon");
        // await dispatch("reRootHierarchy", { parent: roots[0] });
      },

      async refreshHierarchy({ state, commit, dispatch }) {
        function sleep(ms) {
          return new Promise(resolve => setTimeout(resolve, ms))
        }

        // Wait untill classes are ready
        while (!state.classesReady) {
          await sleep(1000)
        }

        if (state.selectedClasses.length !== 0) {
          await dispatch('loadRoot')
        }
      },

      async loadRoot({ state, commit, dispatch }) {
        commit('beginLoadingRoot')

        try {
          const roots = await this.$axios.$post('/nr2/rpc/get_root', {
            id_classes: '{' + state.selectedClasses.join(',') + '}'
          })

          await dispatch('reRootHierarchy', { parent: roots[0] })
        } catch (e) {
          console.log(e)
        }

        commit('endLoadingRoot')
      },

      async reRootHierarchy({ commit, dispatch }, { parent }) {
        if (parent === undefined) return
        parent.level = 0
        await dispatch('expandHierarchy', { parent })
      },

      async expandHierarchy({ commit, dispatch }, { parent }) {
        commit('beginUpdateOfHierarchy', { parent })
        const children = await dispatch('getChildren', { parent })
        commit('endUpdateOfHierarchy', { children })
      },

      async getChildren({ state }, { parent }) {
        try {
          return await this.$axios.$post('/nr2/rpc/get_children', {
            id_parent: parent.id_taxon,
            id_classes: '{' + state.selectedClasses.join(',') + '}'
          })
        } catch (e) {
          console.log(e)
        }
      }
    }
  })
}

export default createStore
